// integration test
const expect = require("chai").expect
const request = require("request")
const app = require("../src/server")

const { assert } = require('chai')
const port = 3000

describe("Color Code Converter API", () => {

    before("", () => {
        console.log("Starting tests")
    })

    before("Start server before running the tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost${port}`)
            done()
        })
    })

    describe("RGB to Hex conversion", () => {
        const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200)
                done()
            })
        })
        it("return the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("ffffff")
                done()
            })
        })
    })

    describe("Hex to RGB conversion", () => {
        const url = `http://localhost:${port}/hex-to-rgb?hex=ff00ff`

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200)
                done()
            })
        })
        it("return the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("[255,0,255]")
                done()
            })
        })
    })

    after("Stop server after running the tests", (done) => {
        server.close()
        done()
    })
})