// src/server.js
const express = require("express")
const converter = require("./converter")

const app = express()
const port = 3000

app.get('/', (req, res) => res.send("Hello"))

app.get('/rgb-to-hex', (req, res) => {
    res.send(
        converter.rgbToHex(
            parseInt(req.query.red, 10),
            parseInt(req.query.green, 10),
            parseInt(req.query.blue, 10)
            )
        )
})

app.get('/hex-to-rgb', (req, res) => {
    res.send(
        converter.hexToRgb(req.query.hex)
    )
})

if (process.env.NODE_ENV === "test"){
    module.exports = app
} else {
    app.listen(port, () => console.log(`Server: localhost:${port}`))
}

