// src/converter.js

/**
 * Padding outputs two characters always
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex)
}

module.exports = {
    /**
     * Converts RGB to Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} 
     */
    rgbToHex: (red, green, blue) => {
        //console.log({red, green, blue})
        const redHex = red.toString(16) // 0-255 -> 0-ff
        const greenHex = green.toString(16) // 0-255 -> 0-ff
        const blueHex = blue.toString(16) // 0-255 -> 0-ff
        const hex = pad(redHex) + pad(greenHex) + pad(blueHex)
        //console.log(hex)
        return  hex // hex string with 6 characters
    },

    hexToRgb: (hex) => {
        hex.charAt(0) == '#' ? hex = hex.substring(1) : null // check if input has a # in beginning and remove it
        hex = hex.match(/.{1,2}/g) // divide the string to an array
        for (let i = 0; i < hex.length; i++) hex[i] = parseInt(hex[i], 16) // parse the values from hexadecimal
        //console.log(hex);
        return hex // return the array
    },
}