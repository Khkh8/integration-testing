# Integration testing assignment


The conversion from hexadecimal to decimal is done by the following code:
```javascript
hex.charAt(0) == '#' ? hex = hex.substring(1) : null 
// check if input has a # in beginning and remove it

hex = hex.match(/.{1,2}/g) 
// divide the string to an array

for (let i = 0; i < hex.length; i++) hex[i] = parseInt(hex[i], 16) 
// parse the values from hexadecimal to decimal
```
### Picture of the Postman
![alt text](postman.PNG "asd")